import pandas as pd, matplotlib.pyplot as plt, numpy as np
from sklearn.externals import joblib
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics.cluster import completeness_score
from sklearn.mixture.gaussian_mixture import GaussianMixture
from scipy.spatial.distance import pdist,squareform
from seaborn import clustermap
from sklearn.model_selection import train_test_split

Label = ['Vote']
nominal = ['Occupation','Main_transportation','Age_group','Most_Important_Issue','Will_vote_only_large_party']
boolians = ['Voting_Time','Financial_agenda_matters','Gender','Married','Looking_at_poles_results']
boolians_dict = {'Yes': 1, 'No': 0, 'Male':1, 'Female': 0, 'After_16:00':1, 'By_16:00':0}
integers = ['Num_of_kids_born_last_10_years','Number_of_valued_Kneset_members','Number_of_differnt_parties_voted_for','Occupation_Satisfaction','Last_school_grades']
cont = ['Avg_monthly_expense_when_under_age_21','AVG_lottary_expanses','Avg_Satisfaction_with_previous_vote',
        'Garden_sqr_meter_per_person_in_residancy_area','Financial_balance_score_(0-1)',
       '%Of_Household_Income','Avg_government_satisfaction','Avg_education_importance',
        'Avg_environmental_importance','Avg_Residancy_Altitude','Yearly_ExpensesK',
       '%Time_invested_in_work','Yearly_IncomeK','Avg_monthly_expense_on_pets_or_plants',
        'Avg_monthly_household_cost','Phone_minutes_10_years','Avg_size_per_room',
        'Weighted_education_rank','%_satisfaction_financial_policy','Avg_monthly_income_all_years',
        'Political_interest_Total_Score','Overall_happiness_score']

correct = ['Number_of_valued_Kneset_members', 'Yearly_IncomeK', 'Overall_happiness_score',
           'Avg_Satisfaction_with_previous_vote', 'Most_Important_Issue', 'Will_vote_only_large_party',
           'Garden_sqr_meter_per_person_in_residancy_area', 'Weighted_education_rank']

mistranslate = ['Financial_balance_score_.0.1.','X.Of_Household_Income','X.Time_invested_in_work','X._satisfaction_financial_policy']
mistranslate_fix = ['Financial_balance_score_(0-1)', '%Of_Household_Income','%Time_invested_in_work','%_satisfaction_financial_policy']

def Prep(d):
    # ---- sample filteration ---- #
    d.rename(columns=dict(zip(mistranslate, mistranslate_fix)), inplace=True)
    # TODO we CANNOT discard samples with too high missing values
    # missing = d[(d.isnull().sum(axis=1) > 3)].index
    # d.drop(missing, inplace=True)

    # ---- Type setting ---- #
    # type set like original cases
    d_original = pd.read_csv("../ElectionsDataOriginalTraining.csv")
    d_onehot = pd.read_csv("../ElectionsDataCorrectedTraining.csv")
    for col in nominal:
        # get dictionary and convert properally
        col_dict = pd.DataFrame([d_original[col], d_onehot[col]], index=range(2)).T.dropna().drop_duplicates().set_index(0).to_dict()[1]
        d.loc[~d[col].isnull(), col] = d[col].dropna().map(col_dict)
        d[col] = d[col].astype(np.float64)
    for col in boolians:
        d[col] = d[col].replace(boolians_dict)

    # ---- outlier ---- #
    print('outlier')
    from outliers import smirnov_grubbs as grubbs
    for col in cont:
        result = grubbs.test(d[col], alpha=0.05)
        d.loc[[ind for ind in d.index if ind not in result.index], col] = np.NAN

    # ----- imputation ---- #
    # we use the training correlative impputation
    corr = d_onehot[cont + integers].corr()
    from scipy.stats import chi2_contingency
    from scipy.spatial.distance import squareform, pdist
    contingency = lambda a, b: pd.crosstab(pd.Categorical(a), pd.Categorical(b))
    pval = squareform(pdist(d_onehot[boolians + nominal].T, lambda u, v: chi2_contingency(contingency(u, v))[1]))
    pval = pd.DataFrame(pval, index=boolians + nominal, columns=boolians + nominal)

    # for each kind of feature we try to correct using a KNN approach with highly correlated features
    # when failing to do so: on continues variables we use mean imputation
    # and on other veriables we use Distribution-based Bootstrapping
    print('imputation')
    from numpy.random import choice
    from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier

    neigh = KNeighborsRegressor(n_neighbors=5)
    for col in cont:
        if (corr[col].drop([col]) > 0.95).any() and d[col].isnull().sum() > 0:
            best = corr.index[(corr[col] > 0.95)]
            # distance based
            tmp = d[best]
            neigh.fit(tmp.dropna()[best].drop(col, axis=1), tmp.dropna()[col])
            tmp = tmp.drop(col, axis=1)[d[col].isnull()].dropna()
            d.loc[tmp.index, col] = neigh.predict(tmp)
        # if we missed anything, or we did not find a partner impute class mean
        d.loc[d[col].isnull(), col] = d[col].mean()

    neigh = KNeighborsClassifier(n_neighbors=5)
    for col in integers:
        if (corr[col].drop([col]) > 0.95).any() and d[col].isnull().sum() > 0:
            best = corr.index[(corr[col] > 0.95)]
            # distance based
            tmp = d[best].dropna()
            neigh.fit(tmp[best].drop(col, axis=1), tmp[col])
            tmp = d[best].drop(col, axis=1)[d[col].isnull()].dropna()
            d.loc[tmp.index, col] = neigh.predict(tmp)
        # if we missed anything, or we did not find a partner
        # imputation via conservation of distribution of the class
        # ------------------ IN THE ORIGINAL DATABASE -------------------#
        dist = d[col].dropna().value_counts(normalize=True)
        d.loc[d[col].isnull(), col] = choice(dist.index, d[col].isnull().sum(), p=dist.values)

    for col in nominal + boolians:
        if (pval[col].drop([col]) < 1e-5).any() and d[col].isnull().sum() > 0:
            # distance based on best correlated
            best = pval.index[(pval[col] < 1e-5)]
            tmp = d[best].dropna()
            neigh.fit(tmp[best].drop(col, axis=1), tmp[col])
            tmp = d[best].drop(col, axis=1)[d[col].isnull()].dropna()
            d.loc[tmp.index, col] = neigh.predict(tmp)
        # when imputing test values we use training mean, or distribution
        dist = d[col].dropna().value_counts(normalize=True)
        d.loc[d[col].isnull(), col] = choice(dist.index, d[col].isnull().sum(), p=dist.values)

    # ---- normalize / scaling ---- #
    # we normalize like we would the -----------  ORIGINAL ---------------- Values!!!
    print('norm')
    # this spesific stage is rellevent to the test set as well
    from sklearn.preprocessing import MinMaxScaler
    to_norm = ['Avg_Satisfaction_with_previous_vote', 'Political_interest_Total_Score', 'Overall_happiness_score',
               'Weighted_education_rank',
               'Avg_monthly_expense_when_under_age_21', 'AVG_lottary_expanses',
               'Garden_sqr_meter_per_person_in_residancy_area',
               'Yearly_ExpensesK', 'Yearly_IncomeK', 'Avg_monthly_expense_on_pets_or_plants',
               'Avg_monthly_household_cost',
               'Phone_minutes_10_years', 'Avg_size_per_room', 'Avg_monthly_income_all_years']
    tmp_col = pd.DataFrame.append(d[to_norm], pd.DataFrame(d_original[to_norm].values,index=range(len(d)+2,len(d)+len(d_original)+2),columns=to_norm).dropna())
    tmp_col = pd.DataFrame(MinMaxScaler().fit_transform(tmp_col), index=tmp_col.index, columns=to_norm)
    d.loc[:, to_norm] = tmp_col.loc[d.index, to_norm]

    unscaled_precents = ['%Time_invested_in_work', '%_satisfaction_financial_policy', 'Last_school_grades']
    d.loc[:, unscaled_precents] = d.loc[:, unscaled_precents] / 100

    unscaled_tenScale = ['Avg_government_satisfaction', 'Avg_education_importance', 'Avg_environmental_importance',
                         'Avg_Residancy_Altitude']
    d.loc[:, unscaled_tenScale] = d.loc[:, unscaled_tenScale] / 10

    # ---- final print W out ---- #
    d.to_csv('ElectionsData_Pred_Features_Corrected.csv')
    return d

def main():

    # --- Data Prep --- #
    d_final = pd.read_csv('ElectionsData_Pred_Features.csv', index_col=0)
    d_final = Prep(d_final)

    # load model from EX3, and make predictions! #
    GBT = joblib.load('../BestModel')
    VotePred = pd.Series(GBT.predict(d_final[correct]))
    curDist = VotePred.value_counts(normalize=True)
    curDist.plot.bar()
    title = 'Election results prediction'
    plt.title(title)
    plt.tight_layout()
    plt.savefig('figs/'+title+'.png')
    plt.clf()

    d_final['Vote'] = VotePred.values
    d_final.Vote.to_csv("ElectionsData_Pred_Results.csv")
    cat = pd.Categorical(d_final.Vote.dropna())
    d_final.loc[~d_final.Vote.isnull(), Label[0]] = cat.codes
    d_final.Vote = d_final.Vote.astype(np.float64)
    num2party = dict(enumerate(cat.categories))

    # Coalition building #
    # for this part we split our new samples to test and train
    d_final_train, d_final_test = train_test_split(d_final, test_size=0.15, stratify=d_final[Label])
    kf = StratifiedKFold(n_splits=3, shuffle=True)  # with cv
    mod_scores = []
    mod_models = []
    for k, (train_index, test_index) in enumerate(kf.split(d_final_train, d_final_train[Label].values.ravel())):
        for n_clusters in [2, 3, 4]:
            mod = GaussianMixture(n_components=n_clusters, n_init=100, init_params='random')
            mod = mod.fit(d_final_train[correct].iloc[train_index])
            mod_pred = mod.predict(d_final_train[correct].iloc[test_index])
            score_mod = completeness_score(d_final_train.Vote.iloc[test_index].values, mod_pred)
            mod_scores.append(score_mod)
            mod_models.append(mod)
        print("[fold {0}] current max score: {1:.5}".format(k, max(mod_scores)))
    model = mod_models[np.argmax(mod_scores)]

    # use model on data
    curDist = d_final_test.Vote.value_counts(normalize=False)
    pred = model.predict(d_final_test[correct])
    n_clusters = len(np.unique(pred))
    for clust in range(n_clusters):
        plt.subplot(n_clusters + 1, 1, clust + 1)
        print_dist = d_final_test.Vote[pred == clust].value_counts()
        print_dist = print_dist.div(curDist)
        print_dist.index = [num2party[x] for x in print_dist.index]
        print_dist.plot.bar()
    plt.suptitle(','.join([str(n_clusters), model._estimator_type]))
    plt.tight_layout()
    plt.savefig('figs/Voters_test.png')
    plt.clf()
    clustermap(pd.DataFrame(squareform(pdist(model.means_))))
    plt.savefig('figs/Voters_test_clust.png')
    plt.clf()

    # Check sanity with test #
    d_test = pd.read_csv("../ElectionsDataCorrectedTest.csv")
    pred = model.predict(d_test[correct])
    curDist = d_test.Vote.value_counts(normalize=False)
    n_clusters = len(np.unique(pred))
    for clust in range(n_clusters):
        plt.subplot(n_clusters + 1, 1, clust + 1)
        print_dist = (d_test.Vote[pred == clust].value_counts().div(curDist))
        print_dist.plot.bar()
    plt.suptitle(','.join([str(n_clusters), model._estimator_type]))
    plt.tight_layout()
    plt.savefig('figs/test.png')
    plt.clf()

if __name__ == '__main__':
    import sys
    sys.exit(main())