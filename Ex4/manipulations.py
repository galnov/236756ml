Label = ['Vote']
nominal = ['Occupation','Main_transportation','Age_group','Most_Important_Issue','Will_vote_only_large_party']
boolians = ['Voting_Time','Financial_agenda_matters','Gender','Married','Looking_at_poles_results']
boolians_dict = {'Yes': 1, 'No': 0, 'Male':1, 'Female': 0, 'After_16:00':1, 'By_16:00':0}
integers = ['Num_of_kids_born_last_10_years','Number_of_valued_Kneset_members','Number_of_differnt_parties_voted_for','Occupation_Satisfaction','Last_school_grades']
cont = ['Avg_monthly_expense_when_under_age_21','AVG_lottary_expanses','Avg_Satisfaction_with_previous_vote',
        'Garden_sqr_meter_per_person_in_residancy_area','Financial_balance_score_(0-1)',
       '%Of_Household_Income','Avg_government_satisfaction','Avg_education_importance',
        'Avg_environmental_importance','Avg_Residancy_Altitude','Yearly_ExpensesK',
       '%Time_invested_in_work','Yearly_IncomeK','Avg_monthly_expense_on_pets_or_plants',
        'Avg_monthly_household_cost','Phone_minutes_10_years','Avg_size_per_room',
        'Weighted_education_rank','%_satisfaction_financial_policy','Avg_monthly_income_all_years',
        'Political_interest_Total_Score','Overall_happiness_score']
all_f = cont+integers+boolians+nominal
correct = ['Number_of_valued_Kneset_members', 'Yearly_IncomeK', 'Overall_happiness_score',
           'Avg_Satisfaction_with_previous_vote','Most_Important_Issue', 'Will_vote_only_large_party',
           'Garden_sqr_meter_per_person_in_residancy_area', 'Weighted_education_rank']
decision_setters = ['Will_vote_only_large_party', 'Number_of_valued_Kneset_members', 'Avg_Satisfaction_with_previous_vote']
voter_background = ['Yearly_IncomeK','Garden_sqr_meter_per_person_in_residancy_area', 'Weighted_education_rank','Overall_happiness_score'] #'Most_Important_Issue'
color_dict = {'Blues':'b', 'Browns':'#A52A2A', 'Greens':'g', 'Greys':'#D3D3D3', 'Oranges':'#FFA500',
              'Pinks':'#FFC0CB', 'Purples':'#800080', 'Reds':'r', 'Turquoises':'#48D1CC', 'Whites':'w', 'Yellows':'y'}

import pandas as pd, matplotlib.pyplot as plt, numpy as np, math
from sklearn.externals import joblib
from seaborn import heatmap
from scipy.stats import norm
from sklearn.feature_selection import f_classif, chi2
from matplotlib.colors import LogNorm

# --- Load the prepared training set --- #
d = pd.read_csv('../ElectionsDataCorrectedTraining.csv', index_col=0)
d_test = pd.read_csv('../ElectionsDataCorrectedTest.csv', index_col=0)
tmp = pd.concat([d,d_test])
cat = pd.Categorical(tmp[Label[0]].dropna())
tmp.loc[~tmp[Label[0]].isnull(), Label[0]] = cat.codes
tmp[Label[0]] = tmp[Label[0]].astype(np.float64)
num2party = dict( enumerate(cat.categories) )
party_names = [num2party[x] for x in range(11)]
party2num = dict(zip(num2party.values(),num2party.keys()))
d.Vote = tmp.Vote.loc[d.index]
d_test.Vote = tmp.Vote.loc[d_test.index]

Parties = np.unique(d.Vote.values).astype(int)
curDist = d_test.Vote.astype(int).value_counts(normalize=False)

# ---- load previos model from HW3 ---- #

best = joblib.load('../BestModel')
TestRes = pd.Series(best.predict(d_test[correct]),index=d_test.index, name=Label[0])
TestResProb = best.predict_proba(d_test[correct])

# ---- sweeping manipulations ---- #
def SkewToDominate(col,num):
    if col in cont:
        return norm.rvs(loc=d.loc[:, col].max(), scale=(d.loc[:, col].var()) * 0.5, size=num)
    else:
        return d.loc[:, col].max()

def SkewToLesser(col,num):
    if col in cont:
        return norm.rvs(loc=d.loc[:, col].min(), scale=(d.loc[:, col].var()) * 0.5, size=num)
    else:
        return d.loc[:, col].min()

# ----- Get leading features ----- #
correct_cont = [f for f in correct if f not in (nominal+boolians)]
correct_nominals = [f for f in correct if f in (nominal+boolians)]
party_list = d.Vote.unique()
party_index = [num2party[x] for x in d.Vote.unique()]
xnom = pd.DataFrame([pd.Series(chi2(d[correct_nominals],d.Vote == iclass)[1], index=correct_nominals) for iclass in party_list], index=party_index)
xcont = pd.DataFrame([pd.Series(f_classif(d[correct_cont],d.Vote == iclass)[1], index=correct_cont) for iclass in party_list], index=party_index)
xcont.replace(0,min(1e-200,xnom.min().min()), inplace=True)
p = pd.concat([xnom,xcont],axis=1)
minimal = p.min().min(); maximal = p.max().max()
log_norm = LogNorm(vmin=minimal, vmax=maximal)
cbar_ticks = [math.pow(10, i) for i in range(math.floor(math.log10(minimal)), 1 + math.ceil(math.log10(maximal)))]
cbar_ticks = cbar_ticks[0::math.floor(len(cbar_ticks)/10)]
g = heatmap(p.T, norm=log_norm, cbar_kws={"ticks": cbar_ticks})
g.set_yticklabels(g.get_yticklabels(),rotation=0); g.set_xticklabels(g.get_xticklabels(),rotation=90)
plt.title('Pvals')
plt.tight_layout()
plt.savefig('figs/lel.png')
reasnables = p.idxmin(axis=1).unique().tolist()

# --- manipulate single (Automatic detection) --- #
"""
On continues features use one of the above methods. On nominals, for every value: try setting setting that value.
"""
precent = 0.6; plt.figure()
reasnables_cont = [f for f in reasnables if f not in (nominal+boolians)]
reasnables_nominals = [f for f in reasnables if f in (nominal+boolians)]
def quickDistDraw(dist,title):
    dist.plot.bar()
    plt.title(title)
    plt.tight_layout()
    plt.savefig('figs/'+title+'.png')
    plt.clf()
for manipulation in [SkewToDominate, SkewToLesser, 'setVal']:
    manip_name = manipulation.__name__ if manipulation is not 'setVal' else 'setVal'
    target = d_test.sample(frac = precent).index
    if manipulation is not 'setVal':
        for col in reasnables_cont:
            d_tmp = d_test.copy()
            d_tmp.loc[target, col] = manipulation(col ,len(target))
            pred = pd.Series(best.predict(d_tmp[correct]))
            curDist = pred.value_counts(normalize=True)
            quickDistDraw(curDist, '_'.join([manip_name,col]))
    else:
        nominalSetVals = [str(reasnables_nominals[i])+'_'+str(item) for i,sublist in enumerate([np.unique(d[col])
                          for col in reasnables_nominals]) for item in sublist]
        for col in reasnables_nominals:
            for val in np.unique(d[col].values):
                d_tmp = d_test.copy()
                d_tmp.loc[target, col] = val
                pred = pd.Series(best.predict(d_tmp[correct]))
                curDist = pred.value_counts(normalize=True)
                quickDistDraw(curDist, '_'.join([manip_name, str(col), str(val)]))

# ------ manipulate multiple selection (manual selection from reasnables matrix)  ------ #
target = d_test.sample(frac = precent).index
d_combo = d_test.copy()
d_combo.loc[target, 'Will_vote_only_large_party'] = 1
d_combo.loc[target, 'Number_of_valued_Kneset_members'] = SkewToDominate('Number_of_valued_Kneset_members' ,len(target))
plt.figure()
pred = pd.Series(best.predict(d_combo[correct]))
curDist = pred.value_counts(normalize=True)
curDist.plot.bar()
plt.title('To solid block')
plt.tight_layout()
plt.savefig('massive_shift.png')
