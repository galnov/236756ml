Label = ['Vote']
nominal = ['Occupation','Main_transportation','Age_group','Most_Important_Issue','Will_vote_only_large_party']
boolians = ['Voting_Time','Financial_agenda_matters','Gender','Married','Looking_at_poles_results']
boolians_dict = {'Yes': 1, 'No': 0, 'Male':1, 'Female': 0, 'After_16:00':1, 'By_16:00':0}
integers = ['Num_of_kids_born_last_10_years','Number_of_valued_Kneset_members','Number_of_differnt_parties_voted_for','Occupation_Satisfaction','Last_school_grades']
cont = ['Avg_monthly_expense_when_under_age_21','AVG_lottary_expanses','Avg_Satisfaction_with_previous_vote',
        'Garden_sqr_meter_per_person_in_residancy_area','Financial_balance_score_(0-1)',
       '%Of_Household_Income','Avg_government_satisfaction','Avg_education_importance',
        'Avg_environmental_importance','Avg_Residancy_Altitude','Yearly_ExpensesK',
       '%Time_invested_in_work','Yearly_IncomeK','Avg_monthly_expense_on_pets_or_plants',
        'Avg_monthly_household_cost','Phone_minutes_10_years','Avg_size_per_room',
        'Weighted_education_rank','%_satisfaction_financial_policy','Avg_monthly_income_all_years',
        'Political_interest_Total_Score','Overall_happiness_score']
all_f = cont+integers+boolians+nominal
correct = ['Number_of_valued_Kneset_members', 'Yearly_IncomeK', 'Overall_happiness_score',
           'Avg_Satisfaction_with_previous_vote','Most_Important_Issue', 'Will_vote_only_large_party',
           'Garden_sqr_meter_per_person_in_residancy_area', 'Weighted_education_rank']
decision_setters = ['Will_vote_only_large_party', 'Number_of_valued_Kneset_members', 'Avg_Satisfaction_with_previous_vote']
voter_background = ['Yearly_IncomeK','Garden_sqr_meter_per_person_in_residancy_area', 'Weighted_education_rank','Overall_happiness_score'] #'Most_Important_Issue'
color_dict = {'Blues':'b', 'Browns':'#A52A2A', 'Greens':'g', 'Greys':'#D3D3D3', 'Oranges':'#FFA500',
              'Pinks':'#FFC0CB', 'Purples':'#800080', 'Reds':'r', 'Turquoises':'#48D1CC', 'Whites':'w', 'Yellows':'y'}

import pandas as pd, matplotlib.pyplot as plt, numpy as np
from mpl_toolkits.mplot3d import axes3d
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics.cluster import completeness_score
from sklearn.mixture.gaussian_mixture import GaussianMixture
from sklearn.preprocessing import MinMaxScaler
from seaborn import clustermap
from  sklearn.metrics import silhouette_samples
from sklearn.metrics.pairwise import pairwise_distances
from scipy.spatial.distance import pdist,squareform
from sklearn.qda import QDA


# --- Load the prepared training set --- #
d = pd.read_csv('../ElectionsDataCorrectedTraining.csv', index_col=0)
d_test = pd.read_csv('../ElectionsDataCorrectedTest.csv', index_col=0)
tmp = pd.concat([d,d_test])
cat = pd.Categorical(tmp[Label[0]].dropna())
tmp.loc[~tmp[Label[0]].isnull(), Label[0]] = cat.codes
tmp[Label[0]] = tmp[Label[0]].astype(np.float64)
num2party = dict( enumerate(cat.categories) )
party_names = [num2party[x] for x in range(11)]
party2num = dict(zip(num2party.values(),num2party.keys()))
d.Vote = tmp.Vote.loc[d.index]
d_test.Vote = tmp.Vote.loc[d_test.index]

Parties = np.unique(d.Vote.values).astype(int)
curDist = d_test.Vote.astype(int).value_counts(normalize=False)

# ---- cool plot ---- #

to_clust = decision_setters[:3]
fig = plt.figure()
ax = fig.gca(projection='3d')
for i in range(len(Parties)):
    cur = d[d.Vote == i]
    ax.scatter(cur[to_clust[0]], cur[to_clust[1]], cur[to_clust[2]], label=num2party[i], c=color_dict[num2party[i]], alpha=0.5)
ax.set_zlabel(to_clust[2])
plt.xlabel(to_clust[0])
plt.ylabel(to_clust[1])
plt.legend()
plt.title('Party Separation')
plt.tight_layout()
plt.savefig('Class_Party_Separation.png')


# ------ normalize features for clustering ----- #
scale = MinMaxScaler()
for col in [x for x in correct if x in integers+nominal]:
    tmp[col] = scale.fit_transform(tmp[col])
    d[col] = tmp[col].loc[d.index]
    d_test[col] = tmp[col].loc[d_test.index]

del tmp,cat

# --- coalition building clustering --- #
kf = StratifiedKFold(n_splits=3, shuffle=True) # with cv
plt.figure(figsize=(10,10))

mod1 = QDA()
mod1_scores = []
mod1_models = []
mod2_scores = []
mod2_models = []
feature_set = correct; set_name = 'correct'
for k, (train_index, test_index) in enumerate(kf.split(d, d[Label].values.ravel())):
    mod1 = mod1.fit(d[feature_set].iloc[train_index], d.Vote.iloc[train_index])
    mod1_pred = mod1.predict(d[feature_set].iloc[test_index])
    score_mod1 = completeness_score(d.Vote.iloc[test_index].values, mod1_pred)
    mod1_scores.append(score_mod1)
    mod1_models.append(mod1)

    best_fold = []
    for n_clusters in [2, 3, 4]:
        mod2 = GaussianMixture(n_components=n_clusters, n_init=100, init_params='random')
        mod2 = mod2.fit(d[feature_set].iloc[train_index])
        mod2_pred = mod2.predict(d[feature_set].iloc[test_index])
        score_mod2 = completeness_score(d.Vote.iloc[test_index].values, mod2_pred)
        mod2_scores.append(score_mod2)
        best_fold.append(score_mod2)
        mod2_models.append(mod2)
    print("[fold {0}] mod1 score: {1:.5}, mod2 score: {2:.5}".format(k, score_mod1, max(best_fold)))

mod1 = mod1_models[np.argmax(mod1_scores)]
mod2 = mod2_models[np.argmax(mod2_scores)]
for model in [mod1,mod2]:
    pred = model.predict(d_test[feature_set])
    n_clusters = len(np.unique(pred))
    for clust in range(n_clusters):
        plt.subplot(n_clusters+1,1,clust+1) if model == mod2 else plt.subplot(5,3,clust+1)
        print_dist = (d_test.Vote[pred==clust].value_counts().div(curDist))
        print_dist.index = [num2party[x] for x in print_dist.index]
        print_dist.plot.bar()
    plt.suptitle(','.join([str(n_clusters), model._estimator_type]))
    plt.tight_layout()
    plt.savefig('figs/'+str(model._estimator_type)+'_'+set_name)
    plt.clf()
    clustermap(pd.DataFrame(squareform(pdist(model.means_))))
    plt.savefig('figs/'+str(model._estimator_type)+'_'+set_name+'_clust')
    plt.clf()

# ---- direct pairwise coalition building  ---- #
# define each party as a cluster. measure the silhouette
# score for pairwise cluster joining and build a matrix.
dist_list = np.zeros(shape=(len(Parties),len(Parties)))
computed_distance = pairwise_distances(d[feature_set], metric='euclidean')
def silhouette_clust(partya, partyb):
    partya = partya[0]; partyb = partyb[0]
    tmp = ((d.Vote == partya) | (d.Vote == partyb))
    return np.mean(silhouette_samples(computed_distance, tmp, metric='precomputed')[tmp])
dist_list = squareform(pdist(Parties[:,np.newaxis], lambda u,v: silhouette_clust(u,v)))
dist_list = dist_list + np.diag([silhouette_clust([i],[i]) for i in range(len(Parties))])
dist_list = pd.DataFrame(dist_list, index=party_names, columns=party_names)
clustermap(dist_list, annot=True)
plt.suptitle('Clustering of pairwise class joining score\n'
             'score: the mean silhouette of samples in joined cluster\n'
             +set_name)
plt.savefig(set_name+'_pairwise_joining.png')
plt.clf()