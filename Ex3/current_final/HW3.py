import pandas as pd, matplotlib.pyplot as plt, numpy as np
from DataPrep import correct, cont, nominal, integers, boolians, Label

# --- Load the prepared training set --- #
d = pd.read_csv('../ElectionsDataCorrectedTraining.csv', index_col=0)[correct+Label]

# --- Train at least two models --- #
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import accuracy_score as acc_score, roc_auc_score, confusion_matrix
from sklearn.multiclass import OneVsRestClassifier
kf = StratifiedKFold(n_splits=3, shuffle=True) # with cv
mod1 = RandomForestClassifier(n_estimators = 100, criterion='entropy', min_samples_split=5 ,min_samples_leaf=5, max_features=None, n_jobs=-1)
mod1 = OneVsRestClassifier(mod1)
mod2 = GradientBoostingClassifier(min_samples_leaf = 5, max_features=None)

mod1_scores = []
mod1_models = []
mod2_scores = []
mod2_models = []
for k, (train_index, test_index) in enumerate(kf.split(d, d[Label].values.ravel())):
    mod1 = mod1.fit(d[correct].iloc[train_index],d[Label].iloc[train_index].values.ravel())
    mod1_models.append(mod1)
    mod1_pred = mod1.predict(d[correct].iloc[test_index])
    score_mod1 = acc_score(d[Label].iloc[test_index], mod1_pred)
    mod1_scores.append(score_mod1)

    mod2 = mod2.fit(d[correct].iloc[train_index],d[Label].iloc[train_index].values.ravel())
    mod2_models.append(mod2)
    mod2_pred = mod2.predict(d[correct].iloc[test_index])
    score_mod2 = acc_score(d[Label].iloc[test_index], mod2_pred)
    mod2_scores.append(score_mod2)

    print("[fold {0}] mod1 score: {1:.5}, mod2 score: {2:.5}".format(k, score_mod1, score_mod2))

mod1 = mod1_models[np.argmax(mod1_scores)]
mod2 = mod2_models[np.argmax(mod2_scores)]

# --- Load the prepared validation set --- #
d_valid = pd.read_csv('../ElectionsDataCorrectedValid.csv', index_col=0)[correct+Label]

# --- Apply the trained models on the validation set and check performance --- #
mod1_prob = mod1.predict_proba(d_valid[correct])
mod2_prob = mod2.predict_proba(d_valid[correct])

# --- Select the best model for the prediction tasks --- #
mod1_roc_scores = []
mod2_roc_scores = []
class_names = np.unique(d[Label].values)
for i,n_class in enumerate(class_names):
    mod1_roc_scores.append(roc_auc_score(d_valid[Label] == n_class, mod1_prob[:,i]))
    mod2_roc_scores.append(roc_auc_score(d_valid[Label] == n_class, mod2_prob[:,i]))
best = mod1 if np.mean(mod1_roc_scores) > np.mean(mod2_roc_scores) else mod2
print('selected model: '+str(best.__class__.__name__))

# --- Predictions --- #
d_test = pd.read_csv('../ElectionsDataCorrectedTest.csv', index_col=0)
pred = pd.Series(best.predict(d_test[correct]),index=d_test.index, name=Label[0])
pred.to_csv('TestPrediction.csv')

conf = confusion_matrix(pred, d_test[Label])
score = acc_score(pred, d_test[Label])
from seaborn import heatmap
plt.figure()
g = heatmap(pd.DataFrame(conf,index=class_names,columns=class_names))
plt.title('accuracy: '+str(score))
g.set_xticklabels(g.get_xticklabels(),rotation=90)
g.set_yticklabels(g.get_yticklabels(),rotation=0)
plt.tight_layout()
plt.savefig('conf_and_acc.png')

Dist = pred.value_counts(normalize=True)# Predict the division of voters between the various parties
Winner = np.argmax(Dist) # Predict which party will win the majority of votes
plt.figure()
Dist.plot.bar()
plt.tight_layout()
plt.savefig('ElectionResults.png')

"""
On the Election Day, each party would like to suggest transportation services for its
voters. Provide each party with a list of its most probable voters
"""
log = open('transportation_services_log.txt','w')
pred = pd.DataFrame(best.predict_proba(d_test[correct]),columns=class_names)
for n_class in class_names:
    print(n_class+':', file=log)
    print(','.join(pred[n_class][pred[n_class] > 0.95].index.values.astype(str)), file=log)


# --- Class manipulation methods --- #
"""
Bonus C
Identify the factor (voters’ characteristic) which by manipulating you are most likely
to change which party will win the election
"""
from numpy.random import choice
from scipy.stats import norm
def reImputation(col, num):
    FeatureDist = d.loc[:, col].value_counts(normalize=True)
    return choice(FeatureDist.index, num, p=FeatureDist.values) if col in nominal else d[col].mean()

def SkewToDominate(col,num):
    if col in cont:
        return norm.rvs(loc=d.loc[:, col].max(), scale=(d.loc[:, col].var()) * 0.5, size=num)
    else:
        return d.loc[:, col].max()

def SkewToLesser(col,num):
    if col in cont:
        return norm.rvs(loc=d.loc[:, col].min(), scale=(d.loc[:, col].var()) * 0.5, size=num)
    else:
        return d.loc[:, col].min()

"""
For each class and for each feature:
On continues features use one of the above methods. On nominals, for every value: try setting setting that value.
"""
correct_cont = [f for f in correct if f not in (nominal+boolians)]
correct_nominals = [f for f in correct if f in (nominal+boolians)]
for manipulation in [reImputation, SkewToDominate, SkewToLesser, 'setVal']:
    for precent in [0.3,0.6,0.9]:
        target = d_test.sample(frac = precent).index
        best_manip_feature = []
        PossibleDist = []
        if manipulation is not 'setVal':
            for col in correct_cont:
                d_tmp = d_test.copy()
                d_tmp.loc[target, col] = manipulation(col ,len(target))
                pred = pd.Series(best.predict(d_tmp[correct]))
                curDist = pred.value_counts(normalize=True)
                PossibleDist.append(curDist)
                best_manip_feature.append(curDist[Winner] if np.argmax(curDist)!=Winner else np.nan)
        else:
            nominalSetVals = [str(correct_nominals[i])+'_'+str(item) for i,sublist in enumerate([np.unique(d[col])
                              for col in correct_nominals]) for item in sublist]
            for col in correct_nominals:
                for val in np.unique(d[col].values):
                    d_tmp = d_test.copy()
                    d_tmp.loc[target, col] = val
                    pred = pd.Series(best.predict(d_tmp[correct]))
                    curDist = pred.value_counts(normalize=True)
                    PossibleDist.append(curDist)
                    best_manip_feature.append(curDist[Winner] if np.argmax(curDist) != Winner else np.nan)

        if not all(np.isnan(best_manip_feature)):
            manip_name = manipulation.__name__ if manipulation is not 'setVal' else 'setVal'
            print(str(precent) + ' ' + manip_name)
            MostDifferFeature = (correct_cont if manipulation is not 'setVal' else nominalSetVals)[np.nanargmin(best_manip_feature)]
            print('Most valued feature is '+str(MostDifferFeature), end=' ')
            print('lowering predicted wining party vote rate to '+str(np.nanmin(best_manip_feature)))
            plt.figure()
            PossibleDist[np.nanargmin(best_manip_feature)].plot.bar()
            plt.title(str(precent)+' '+MostDifferFeature+' '+manip_name+' manipulation')
            plt.tight_layout()
            plt.savefig('Most_Effective_'+str(precent)+'_'+manip_name+'_Manipulation.png')