import pandas as pd, matplotlib.pyplot as plt, numpy as np
Label = ['Vote']
nominal = ['Occupation','Main_transportation','Age_group','Most_Important_Issue','Will_vote_only_large_party']
boolians = ['Voting_Time','Financial_agenda_matters','Gender','Married','Looking_at_poles_results']
boolians_dict = {'Yes': 1, 'No': 0, 'Male':1, 'Female': 0, 'After_16:00':1, 'By_16:00':0}
integers = ['Num_of_kids_born_last_10_years','Number_of_valued_Kneset_members','Number_of_differnt_parties_voted_for','Occupation_Satisfaction','Last_school_grades']
cont = ['Avg_monthly_expense_when_under_age_21','AVG_lottary_expanses','Avg_Satisfaction_with_previous_vote',
        'Garden_sqr_meter_per_person_in_residancy_area','Financial_balance_score_(0-1)',
       '%Of_Household_Income','Avg_government_satisfaction','Avg_education_importance',
        'Avg_environmental_importance','Avg_Residancy_Altitude','Yearly_ExpensesK',
       '%Time_invested_in_work','Yearly_IncomeK','Avg_monthly_expense_on_pets_or_plants',
        'Avg_monthly_household_cost','Phone_minutes_10_years','Avg_size_per_room',
        'Weighted_education_rank','%_satisfaction_financial_policy','Avg_monthly_income_all_years',
        'Political_interest_Total_Score','Overall_happiness_score']


# ---- sample filteration ---- #
# we discard samples with too high missing values
d = pd.read_csv('../ElectionsData.csv')
missing = d[(d.isnull().sum(axis=1) > 3)].index
d.drop(missing, inplace=True)


# ---- Type setting ---- #
for col in nominal:#+Label:
    d.loc[~d[col].isnull(), col] = pd.Categorical(d[col].dropna()).codes
    d[col] = d[col].astype(np.float64)
for col in boolians:
    d[col] = d[col].replace(boolians_dict)


# ---- split to train-test ---- #
from sklearn.model_selection import train_test_split
d, d_test = train_test_split(d, test_size=0.15)
d_test.to_csv('../ElectionsDataOriginalTest.csv')


# ---- filter correlation + visualization ---- #
# cont / integers
# using simple pearson correlation
from seaborn import heatmap
corr = d[cont+integers].corr()
#g = heatmap(corr)
#g.set_xticklabels(g.get_xticklabels(),rotation=90)
#g.set_yticklabels(g.get_yticklabels(),rotation=0)
#g.set_title('corr',size=20)
#plt.tight_layout()
extream_cont_corr = corr[((np.abs(corr) - np.eye(len(cont+integers)) > 0.95).any())].index
print(extream_cont_corr)

# boolians / nominals
# using chi^2 distribution
from scipy.stats import chi2_contingency
from scipy.spatial.distance import squareform, pdist
contingency = lambda a,b: pd.crosstab(pd.Categorical(a), pd.Categorical(b))
pval = squareform(pdist(d[boolians+nominal].T, lambda u,v: chi2_contingency(contingency(u,v))[1]))
pval = pd.DataFrame(pval,index=boolians+nominal, columns=boolians+nominal)
#g = heatmap(pval)
#g.set_xticklabels(g.get_xticklabels(),rotation=90)
#g.set_yticklabels(g.get_yticklabels(),rotation=0)
#g.set_title('pvals',size=20)
#plt.tight_layout()
extream_pval_corr = pval[((pval + np.eye(len(boolians+nominal))) < 1e-5).any()].index
print(extream_pval_corr)


# ---- outlier ---- #
print('outlier')
from outliers import smirnov_grubbs as grubbs
for col in cont:
    result = grubbs.test(d[col], alpha=0.05)
    d.loc[[ind for ind in d.index if ind not in result.index], col] = np.NAN


# ----- imputation ---- #
# for each kind of feature we try to correct using a KNN approach with highly correlated features
# when failing to do so: on continues variables we use mean imputation
# and on other veriables we use Distribution-based Bootstrapping
print('imputation')
from sklearn.preprocessing import Imputer
from numpy.random import choice
from sklearn.neighbors import KNeighborsRegressor,KNeighborsClassifier

mean_imp = Imputer(axis=1)
neigh = KNeighborsRegressor(n_neighbors=5)
label_values = np.unique(d[Label].values)
for col in cont:
    if (corr[col].drop([col]) > 0.95).any() and d[col].isnull().sum()>0:
        best = corr.index[(corr[col] > 0.95)]
        # distance based
        tmp = d[best]
        neigh.fit(tmp.dropna()[best].drop(col,axis=1), tmp.dropna()[col])
        tmp = tmp.drop(col, axis=1)[d[col].isnull()].dropna()
        d.loc[tmp.index, col] = neigh.predict(tmp)
        post = d[col].isnull().sum()
    # if we missed anything, or we did not find a partner impute class mean
    mean_by_label = d.groupby(Label)[col].mean()
    for label in label_values:
        d.loc[(d[col].isnull()) & (d.Vote == label), col] = mean_by_label[label] #mean_imp.fit_transform(d[col].values.reshape(1, -1).tolist(), d[Label].values.reshape(1, -1).tolist())
    # when imputing test values we use training mean, or distribution
    d_test.loc[d_test[col].isnull(), col] = d[col].mean()

neigh = KNeighborsClassifier(n_neighbors=5)
for col in integers:
    if (corr[col].drop([col]) > 0.95).any() and d[col].isnull().sum()>0:
        best = corr.index[(corr[col] > 0.95)]
        # distance based
        tmp = d[best].dropna()
        neigh.fit(tmp[best].drop(col,axis=1), tmp[col])
        tmp = d[best].drop(col, axis=1)[d[col].isnull()].dropna()
        d.loc[tmp.index,col] = neigh.predict(tmp)

    # if we missed anything, or we did not find a partner
    # imputation via conservation of distribution of the class
    for target_class in np.unique(d[Label].values):
        target_index = d.index[(d[col].isnull()) & (d.Vote == target_class)]
        dist = d.loc[(d.Vote == target_class),col].value_counts(normalize=True)
        d.loc[target_index, col] = choice(dist.index, len(target_index), p=dist.values)
    # when imputing test values we use training mean, or distribution
    dist = d[col].value_counts(normalize=True)
    d_test.loc[d_test[col].isnull(), col] = choice(dist.index, d_test[col].isnull().sum(), p=dist.values)

for col in nominal+boolians:
    if (pval[col].drop([col]) < 1e-5).any() and d[col].isnull().sum()>0:
        # distance based on best correlated
        best = pval.index[(pval[col] < 1e-5)]
        tmp = d[best].dropna()
        neigh.fit(tmp[best].drop(col, axis=1), tmp[col])
        tmp = d[best].drop(col, axis=1)[d[col].isnull()].dropna()
        d.loc[tmp.index, col] = neigh.predict(tmp)
    # if we missed anything, or we did not find a partner
    # imputation via conservation of distribution of the class
    for target_class in np.unique(d[Label].values):
        target_index = d.index[(d[col].isnull()) & (d.Vote == target_class)]
        dist = d.loc[(d.Vote == target_class),col].value_counts(normalize=True)
        d.loc[target_index, col] = choice(dist.index, len(target_index), p=dist.values)
    # when imputing test values we use training mean, or distribution
    dist = d[col].value_counts(normalize=True)
    d_test.loc[d_test[col].isnull(), col] = choice(dist.index, d_test[col].isnull().sum(), p=dist.values)


# ---- normalize / scaling ---- #
print('norm')
# this spesific stage is rellevent to the test set as well
from sklearn.preprocessing import MinMaxScaler
to_norm = ['Avg_Satisfaction_with_previous_vote','Political_interest_Total_Score','Overall_happiness_score','Weighted_education_rank',
           'Avg_monthly_expense_when_under_age_21', 'AVG_lottary_expanses', 'Garden_sqr_meter_per_person_in_residancy_area',
           'Yearly_ExpensesK', 'Yearly_IncomeK', 'Avg_monthly_expense_on_pets_or_plants', 'Avg_monthly_household_cost',
           'Phone_minutes_10_years', 'Avg_size_per_room', 'Avg_monthly_income_all_years']
tmp_col = pd.DataFrame.append(d,d_test)[to_norm]
tmp_col = pd.DataFrame(MinMaxScaler().fit_transform(tmp_col), index=tmp_col.index, columns = to_norm)
d.loc[:, to_norm] = tmp_col.loc[d.index,to_norm]
d_test.loc[:, to_norm] = tmp_col.loc[d_test.index,to_norm]

unscaled_precents = ['%Time_invested_in_work','%_satisfaction_financial_policy', 'Last_school_grades']
d.loc[:, unscaled_precents] = d.loc[:, unscaled_precents] / 100
d_test.loc[:, unscaled_precents] = d_test.loc[:, unscaled_precents] / 100

unscaled_tenScale = ['Avg_government_satisfaction','Avg_education_importance','Avg_environmental_importance','Avg_Residancy_Altitude']
d.loc[:, unscaled_tenScale] = d.loc[:, unscaled_tenScale] / 10
d_test.loc[:, unscaled_tenScale] = d_test.loc[:, unscaled_tenScale] / 10


# ---- split to train-valid ---- #
from sklearn.model_selection import train_test_split
d, d_valid = train_test_split(d, test_size=0.18) #.18*.85=~.15 test and validation are close in size


# ---- Feature redundency  ---- #
print('redundency')
# we first aim to filter out out closely related features
from scipy.sparse.csgraph import connected_components
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from sklearn import svm
from sklearn.feature_selection import SelectKBest, mutual_info_classif, chi2, f_classif

def pd_fill_diagonal(df_matrix, value=0):
    mat = df_matrix.values
    n = mat.shape[0]
    mat[range(n), range(n)] = value
    newdf = pd.DataFrame(mat)
    newdf.columns =df_matrix.columns
    return newdf

featureToRemove = []
# cont & int
highcorrNoDiagonal = pd_fill_diagonal(corr)
highcorrNoDiagonal = highcorrNoDiagonal[highcorrNoDiagonal > 0.95]
adjacency_matrix  = np.where(highcorrNoDiagonal > 0,1,0)
(nClusters, labels) = connected_components(adjacency_matrix)
clusters = [np.where(labels[:] == clusterIndex)[0] for clusterIndex in range(nClusters)]
for cluster in [c for c in clusters if len(c) > 1]:
    clusterCol = [corr.columns[field] for field in cluster]
    mutual = SelectKBest(mutual_info_classif, k=1)
    bestField = mutual.fit(d[clusterCol], d[Label].values.ravel()).scores_.argsort()[-1]
    bestField = d[clusterCol].columns[bestField]
    featureToRemove.extend([feature for feature in clusterCol if (feature != bestField)])

# nominal & boolian
highcorrNoDiagonal = pd_fill_diagonal(pval)
highcorrNoDiagonal = highcorrNoDiagonal[highcorrNoDiagonal < 1e-5]
adjacency_matrix  = np.where(highcorrNoDiagonal > 0,1,0)
(nClusters, labels) = connected_components(adjacency_matrix)
clusters = [np.where(labels[:] == clusterIndex)[0] for clusterIndex in range(nClusters)]
for cluster in [c for c in clusters if len(c) > 1]:
    clusterCol = [pval.columns[field] for field in cluster]
    mutual = SelectKBest(mutual_info_classif, k=1)
    bestField = mutual.fit(d[clusterCol], d[Label].values.ravel()).scores_.argsort()[-1]
    bestField = d[clusterCol].columns[bestField]
    featureToRemove.extend([feature for feature in clusterCol if (feature != bestField)])

Remain = [f for f in (cont+integers+nominal+boolians) if f not in featureToRemove]


# ---- filter method ---- #
print('filter')
from math import ceil
stat = SelectKBest(chi2, k='all')
nonCont = [col for col in Remain if col in (boolians+integers+nominal)]
Filter = stat.fit(d[nonCont], d[Label].values.ravel())
FilterBest = list(np.array(nonCont)[Filter.pvalues_ < 1e-5])

stat = SelectKBest(f_classif, k='all')
RemainCont = [col for col in Remain if col in cont]
Filter = stat.fit(d[RemainCont], d[Label].values.ravel())
FilterBest = FilterBest + list(np.array(RemainCont)[Filter.pvalues_ < 1e-5])


# ---- wrapper method ---- #
print('wrapper')
from sklearn.ensemble import RandomForestClassifier
# tree = RandomForestClassifier(n_estimators=10, criterion='entropy')
knn = RandomForestClassifier(5)
sfs1 = SFS(knn, k_features=(1,10), forward=True, floating=True, verbose=2, scoring='accuracy', cv=5)
sfs1 = sfs1.fit(d[Remain].as_matrix(), d[Label].values.ravel())
WrapperBest = [d[Remain].columns[index] for index in sfs1.k_feature_idx_]


# ---- final print W out ---- #
FinalFeatures = list(set(WrapperBest + FilterBest))
ToPrint = FinalFeatures + Label
d_test[ToPrint].to_csv('../ElectionsDataCorrectedTest.csv')
d_valid[ToPrint].to_csv('../ElectionsDataCorrectedValid.csv')
d[ToPrint].to_csv('../ElectionsDataCorrectedTraining.csv')

form = pd.read_csv('./SelectedFeatures.csv').T
form.loc[FinalFeatures,:] = 1
form.loc[[ind for ind in form.index if ind not in FinalFeatures],:] = 0
form.T.to_csv('../SelectedFeaturesRes.csv')

# remake original dataset
og = pd.read_csv('../ElectionsData.csv')
og.loc[d.index].to_csv('../ElectionsDataOriginalTraining.csv')
og.loc[d_valid.index].to_csv('../ElectionsDataOriginalValidation.csv')

# test success
from sklearn.metrics import confusion_matrix
neigh = KNeighborsClassifier(5)
neigh.fit(d[FinalFeatures],d[Label].values.ravel())
res = neigh.predict(d_valid[FinalFeatures])
print(np.diag(confusion_matrix(res,d_valid[Label])).sum()/len(d_valid))

clf = svm.SVC()
clf.fit(d[FinalFeatures],d[Label].values.ravel())
res = clf.predict(d_valid[FinalFeatures])
print(np.diag(confusion_matrix(res,d_valid[Label])).sum()/len(d_valid))

from sklearn.ensemble import RandomForestClassifier
tree = RandomForestClassifier(criterion='entropy')
tree.fit(d[FinalFeatures],d[Label].values.ravel())
res = tree.predict(d_valid[FinalFeatures])
print(np.diag(confusion_matrix(res,d_valid[Label])).sum()/len(d_valid))
print('yo')